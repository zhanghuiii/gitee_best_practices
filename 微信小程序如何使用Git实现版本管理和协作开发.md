微信小程序开发的过程中，代码版本管理往往需要使用第三方工具进行管理。虽然微信 Web 开发工具提供了对 Git 文件版本状态的提示，但实际的使用体验依然不尽人意。    
  
随着微信 Web 开发工具的更新，最新的内测版本已经支持 Git 的直接管理，本文将就在微信 Web 开发工具中使用 Git 做版本管理做详细介绍。   

### 环境准备  
开发环境：Mac/Windows/Linux均可
开发工具：微信Web开发者工具Beta版本、Git
Git托管服务：码云
使用Git服务需要在系统上先安装好Git，相关Git环境的安装，详见Git入门和Git的安装。

由于目前Git管理功能的支持尚在公测阶段，故本文将使用微信Web开发者工具 Beta版本作为示例。

访问 https://developers.weixin.qq.com/miniprogram/dev/devtools/devtools.html 即可下载微信Web开发者工具Beta版本（下文以微信Web开发者工具简称代替）

### 创建小程序项目
使用Git版本管理，首先需要有个git的仓库。打开微信Web开发者工具，新建/打开小程序的项目。此处以新建项目为例，假设项目名为HelloGitee，填写好相应路径和appid，选择建立普通快速启动模板，确认并新建项目。    

![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143048_3fa24698_1899542.jpeg "1.jpg")  

创建完成后，得到了初始化后的项目。  

![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143128_172e48cd_1899542.jpeg "2.jpg")   
 
### 创建远程仓库
在 https://gitee.com 上登陆自己等码云账号，在右上角新建按钮选择**「新建项目」**。填写相应的项目仓库信息。

此处我们选择使用公开的仓库，命名路径为「HelloGitee」，开发语言选择「JavaScript」。确认后点击「创建」按钮初始化远程仓库。  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143152_a461520a_1899542.jpeg "3.jpg")  

创建并初始化完远程仓库后，我们得到了一个空白仓库如下图。项目的仓库地址是：https://gitee.com/normalcoder/HelloGitee.git 接下去我们将初始化本地等Git仓库。  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143210_133695be_1899542.jpeg "4.jpg")  


### 初始化本地仓库
在微信Web开发者工具中点击面板上的「版本管理」按钮，将弹出开发者工具中的版本管理面板。

由于是新建项目，并没有初始化过Git仓库，所以项目会提示初始化Git仓库，点击「初始化 Git 仓库」，点击「确认」，完成本地仓库的初始化。

这一步骤相当于执行「git init」命令。  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143228_b1625568_1899542.jpeg "5.jpg")  

初始化完成后，我们可以看到本地的仓库和当前的Git状态。下图为微信Web开发者工具初始化后的版本控制面板。
  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143244_be1619ec_1899542.jpeg "6.jpg")    

### 配置仓库信息
初始化完成后，依次点击「工作空间」->「设置」->「通用」->「编辑」，编辑在Git中使用的用户名和邮箱。这一步相当于git config命令中的配置操作。

$ git config --global user.name "用户名"
$ git config --global user.email "邮箱"
需要注意的是：此处配置的邮箱名需要和 码云 https://gitee.com 上的邮箱保持一致，才能保证提交后能统计到Git的提交贡献信息。  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143424_0f3ad19c_1899542.jpeg "7.jpg")     
![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143440_933c5453_1899542.jpeg "8.jpg")   
    
切换到仓库设置的「远程」选项卡，这时候会发现提示「未找到远程仓库信息」，点击「添加」，将前面创建的远程仓库地址填进去，仓库名称此处命名为「master」，可自行命名。  

![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143504_461e09fb_1899542.jpeg "9.jpg")  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143520_dbbff9f0_1899542.jpeg "10.jpg")  

添加完成后即可看到项目中的远程分支信息。  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143545_3c67fd97_1899542.jpeg "11.jpg")  

### 推送代码到远程仓库
点击操作面板上的「推送按钮」，在弹出窗口选择「推送到新的远程仓库分支」，名称填写「master」，表示推送到远程仓库的master分支，然后点击「确定」。
![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143601_8c11aa5d_1899542.jpeg "12.jpg")  

推送完成后，我们可以顺利的看到「远程仓库」下出现了分支信息。访问码云上的仓库，也能看到推送过去的信息。此时我们已经完成了代码从本地仓库到远程仓库到推送。    

![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143620_9a2d690a_1899542.jpeg "13.jpg")  

![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143635_8afa0906_1899542.jpeg "14.jpg")  

### 注意事项 和 身份授权设置
在推送的时候如果遇到了提示推送失败，需要检查用户授权，表示可能微信Web开发者工具并没有读取到本地用户的ssh授权配置，需要在开发工具中设置用户的授权信息。  
![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143653_3fda2317_1899542.jpeg "15.jpg")  


初始化完成后，依次点击「工作空间」->「设置」->「网路与认证」->「认证方式」，可以选择远程仓库的认证方式，默认为「自动」。

选择「输入用户名和密码」，在下方输入码云的账号和密码后，再次执行推送操作即可。
![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143715_8ccd78b5_1899542.jpeg "16.jpg")  


### 修改并提交代码
接下来我们来修改我们的项目代码。

点击开发工具面板的「版本管理」按钮，关闭「版本管理」面板，打开「pages/index/index.wxml」，修改其中内容「获取头像昵称」为「我的第一次修改提交」，保存。  

![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143732_72861280_1899542.jpeg "17.jpg")  

再次切换到「版本管理」面板，可以看到当前本地分支有一个文件等待提交，选中并勾选文件，可以查看当前文件内发生的改动。  

![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143752_cfd03f58_1899542.jpeg "18.jpg")  

在下方提交框填写提交的备注信息，点击「提交」，将代码提交到本地仓库主干分支上。提交后，可以在本地仓库分支上查看提交记录。  

![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143814_b62fc0ab_1899542.jpeg "19.jpg")   

接下去我们再次将代码从本地分支推送到远程仓库。点击操作面板上的「推送按钮」，在弹出窗口选择「推送到一下远程仓库分支」，选择一存在的远程master仓库的master分支，然后点击「确定」。
![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143834_6f37c304_1899542.jpeg "20.jpg")  


推送完成后，即可在码云对应的仓库主页看到提交的代码变更。
![输入图片说明](https://images.gitee.com/uploads/images/2019/0517/143848_9209c2dd_1899542.jpeg "21.jpg")   


### 总结
在上面的操作中，我们通过微信Web开发者工具的版本管理功能，对小程序的代码进行了版本的管理控制，并提交到了远程的Git仓库中。

在实际的项目开发中，我们还可以充分利用Git在版本管理和协作上的特性，灵活的和他人进行协作，进而规范代码管理，更高效的进行协作开发。